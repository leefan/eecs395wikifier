#!/usr/bin/python
import sys, re, os, cgi, cgitb, urllib, difflib
from BeautifulSoup import BeautifulSoup, SoupStrainer

form = cgi.FieldStorage()
inputtext = urllib.quote(form.getvalue('inputtext'))
context = urllib.quote(form.getvalue('context'))

d = difflib.Differ()
wikitext = '%7Bwikicontent%7D'+inputtext+'%7B%2Fwikicontent%7D'+context
cmd = 'sudo wget -q --post-data="text=' + wikitext + '" http://cogcomp.cs.illinois.edu/demo/wikify/results.php -O wikitemp'
cmdb = 'sudo wget -q --post-data="text=' + inputtext + '" http://cogcomp.cs.illinois.edu/demo/wikify/results.php -O wikitemp2'
os.system(cmd)
os.system(cmdb)
f = open('wikitemp','r')
fb = open('wikitemp2', 'r')
contents = re.compile('<div id="results">(.*?)</div>', re.DOTALL | re.IGNORECASE).findall(f.read())[0]
withcontext = re.compile('{wikicontent}(.*?){/wikicontent}', re.DOTALL | re.IGNORECASE).findall(contents)[0]
withoutcontext = re.compile('<div id="results">(.*?)</div>', re.DOTALL | re.IGNORECASE).findall(fb.read())[0]
print 'Content-type: text/html\n'
print '<html><body>'
print '<hr />With Context<hr />'
print withcontext
print '<br /><br />'
print '<hr />Without Context<hr />'
print withoutcontext
print '<br /><br />'
print '<hr />Diffs of Links and Content<hr />'
lista = ''
listb = ''
for link in BeautifulSoup(withcontext, parseOnlyThese=SoupStrainer('a')):
	if link.has_key('href'):
		lista += link['href'] + '\n'
print '<br /><br />'
for link in BeautifulSoup(withoutcontext, parseOnlyThese=SoupStrainer('a')):
	if link.has_key('href'):
		listb += link['href'] + '\n'
diffurl = d.compare(listb.splitlines(),lista.splitlines())
print '<br />'.join(diffurl)

print '<br /><br />'
diff = d.compare(withoutcontext.splitlines(),withcontext.splitlines())
print '<br />'.join(diff)
print '</body></html>'
f.close()
fb.close()
